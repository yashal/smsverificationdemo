package com.example.zolo13.smsverificationdemo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("generate-otp.json")
    Call<LoginPojo> sendOTP(@Field("primaryContact") String primaryContact);

}
